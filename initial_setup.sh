sudo apt-get update && sudo apt-get install git
git clone git@bitbucket.org:JosueAChavez/bitcamp-2019.git
cd bitcamp-2019
sudo apt-get install python-pip
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
#sudo apt-get -y install python-rpi.gpio
pip install RPi.GPIO
pip install twilio
pip install flask
pip install virtualenv
sudo apt-get install virtualenv 

# create and activate your virtual environment
virtualenv --no-site-packages .
bin/pip install -r requirements.txt

# activate the virtual environment
source bin/activate

# if running sense_bell.py doesnt work becuase of accesing pins, look at this:
#  https://raspberrypi.stackexchange.com/questions/40105/access-gpio-pins-without-root-no-access-to-dev-mem-try-running-as-root

unzip ngrok-stable-linux-arm.zip
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-arm.zip
./ngrok authtoken 5A2pH1bAFa4TMqZGN5uCA_64MK7qNkWCPSssmPZFF7q

