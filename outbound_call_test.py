# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'AC5bb5c891318a510e7be40234ccd5b47f'
auth_token = '5ca922d34f2a6fed2defcf84deb1fc3b'
client = Client(account_sid, auth_token)

call = client.calls.create(
            url='http://demo.twilio.com/docs/voice.xml',
            to='+12405502893',
            from_='+12407710030'
        )

print(call.sid)
