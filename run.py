# /usr/bin/env python
# Download the twilio-python library from twilio.com/docs/libraries/python
from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
from send_mms import send_mms

app = Flask(__name__)

@app.route("/sms", methods=['GET', 'POST'])
def sms_ahoy_reply():
    """Respond to incoming messages with a friendly SMS."""
    
    body = request.values.get('Body', None)    
    
    # Start our response
    resp = MessagingResponse()
    
    #Checks body create a custom reponse
    if u'\U0001f44d' in body:
        resp.message(u'\U0001f44c')
    elif "fuck" in body.lower():
        resp.message(u'\U0001f595')
    elif u'\U0001f595' in body:
        resp.message("Thats not very nice...")
    elif "U0001f3ff" in body.encode('raw_unicode_escape'):
        resp.message("diversity!!!")
    elif u'\U0001f648' in body and u'\U0001f649' in body and u'\U0001f64a' in body:
        resp.message("See no evil, Hear no evil, Said no evil")
    elif u'\U0001f648' in body:
        resp.message("See no evil")
    elif u'\U0001f649' in body:
        resp.message("Hear no evil")
    elif u'\U0001f64a'in body:
        resp.message("Said no evil")
    elif "cat" in body.lower():
        send_mms("Here's a picture of Lil Chonk", "cat")
    elif "logan" in body.lower() or "dog" in body.lower():
        send_mms("Here's a picture of Logan", "dog")
    else:
        resp.message(body)
    
    send_mms(body, "")

    #prints whats in body, for debugging
    print body.encode('raw_unicode_escape')

    # Writes to dinner.txt what "food" is   
    with open('dinner.txt', 'w+') as f:   
        f.write(body.encode('raw_unicode_escape')) 
    
    # prints whats in body, used to debugging purposes 
    print("Testing", body)
   
    return str(resp)


if __name__ == "__main__":
    app.run(debug=True)
