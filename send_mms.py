# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
import json

def send_mms(message_body, item_type):
    with open("em_config.json", "r") as f:
        em_config = json.load(f)

    account_sid = em_config["account_sid"]
    auth_token = em_config["auth_token"]
    client = Client(account_sid, auth_token)
    
    # Sets url to a default picture
    url='https://i.ytimg.com/vi/lFjWA5w74nY/maxresdefault.jpg'
    
    # Loop creates a message for each number in "recipients"
    for phone_num in em_config["recipients"]:
        # Will assign unique image address to url based on item_type
        if "dog" in item_type.lower():
            url='http://erwinmendez.com/img/portfolio/fullsize/1.jpg'
        elif "cat" in item_type.lower():
            url='https://i.redd.it/fxpbgq5myls21.png'
        else:
            message_body += "\n***Somehow this happned, but it shouldn't have. Check source code***"
            url='https://i.ytimg.com/vi/lFjWA5w74nY/maxresdefault.jpg'
        
        # Creates message
        message = client.messages.create(
            body=message_body,
            from_=em_config["twilio_number"],
            media_url=url,
            to=phone_num)

        print("Message sent to " + phone_num + " and message sid is: " + message.sid)
