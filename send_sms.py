from twilio.rest import Client
import json

def send_sms(message_body):
	with open("config.json", "r") as f:
		config = json.load(f)

	# Your Account SID from twilio.com/console
	account_sid = config["account_sid"]
	# Your Auth Token from twilio.com/console
	auth_token  = config["auth_token"]

	client = Client(account_sid, auth_token)
        for phone_num in config["recipients"]:
	    message = client.messages.create(
	    	    to=phone_num, 
	    	    from_=config["twilio_number"],
		    body=message_body)

            print("Sent message to " + phone_num + "\nmessage.sid: " + message.sid)
