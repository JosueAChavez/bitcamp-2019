import RPi.GPIO # can only run this on pi
import time

from send_sms import send_sms


#GPIO SETUP
channel = 17
RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setup(channel, RPi.GPIO.IN)

def callback(channel):
        if RPi.GPIO.input(channel):
                with open("dinner.txt") as f:
                    food = f.readline().rstrip()
                print("Dinner is ready! We're having " + food)
                send_sms("Dinner is ready! We're having " + food)
                time.sleep(20) # don't want to spam with messages
                print "Ready to listen again"


RPi.GPIO.add_event_detect(channel, RPi.GPIO.BOTH, bouncetime=300)  # let us know when the pin goes HIGH or LOW
RPi.GPIO.add_event_callback(channel, callback)  # assign function to GPIO PIN, Run function on change

print("Ready to listen for the bell to ring")
# infinite loop
while True:
        time.sleep(1)
